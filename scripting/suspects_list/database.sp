/*** Databes ***/
void DatabesConnect()
{
    if (SQL_CheckConfig("suspects_list")) Database.Connect(OnDatabaseConnection, "suspects_list", true);
    else
    {
        KeyValues hKeyValues = new KeyValues(NULL_STRING);
        hKeyValues.SetString("driver", "sqlite");
        hKeyValues.SetString("database", "suspects_list");

        char szError[256];
        g_hDatabase = SQL_ConnectCustom(hKeyValues, SZF(szError), false);
        delete hKeyValues;
    
        OnDatabaseConnection(g_hDatabase, szError, false);
    }
}

public void OnDatabaseConnection(Database hDatabase, const char[] szError, any MySQL)
{
    if (hDatabase == null || szError[0])
    {
        SetFailState("DatabaseConnect: %s", szError);
        return;
    }

    g_hDatabase = hDatabase;

    if (!MySQL) g_bMySQL = false;
    else
    {
        char szDriver[8];
        g_hDatabase.Driver.GetIdentifier(SZF(szDriver));
        g_bMySQL = (szDriver[0] == 'm');
    }

    if (g_bMySQL)
    {
        g_hDatabase.Query(SQL_CreateTable_Callback,	"CREATE TABLE IF NOT EXISTS `suspects_list` ( \
														`steam_id` VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, \
    													`add_time` INT UNSIGNED NOT NULL DEFAULT '0', \
    													`reason` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, \
    													`add_adm_name` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, \
    													`add_adm_steam_id` VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, \
    													`ban_time` INT UNSIGNED NOT NULL DEFAULT '0', \
                                                        `susp_name` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, \
														PRIMARY KEY (`steam_id`) \
													) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;");
    }
    else
    {
        g_hDatabase.Query(SQL_CreateTable_Callback,	"CREATE TABLE IF NOT EXISTS `suspects_list` ( \
                                                	`steam_id` VARCHAR(64) NOT NULL PRIMARY KEY, \
                                                	`add_time` INTEGER UNSIGNED NOT NULL default 0, \
                                                	`reason` VARCHAR(128) NOT NULL default 'unknown', \
                                                	`add_adm_name` VARCHAR(128) NOT NULL default 'unknown', \
                                                	`add_adm_steam_id` VARCHAR(64) NOT NULL default 'unknown', \
                                                	`ban_time` INTEGER UNSIGNED NOT NULL default 0, \
                                                    `susp_name` VARCHAR(128) NOT NULL default 'unknown')");
    }
}

public void SQL_CreateTable_Callback(Database hDatabase, DBResultSet hResult, const char[] szError, any data)
{
    if (hResult == null || szError[0])
    {
        SetFailState("SQL_CreateTable_Callback: %s", szError);
        return;
    }

    if(!g_bMySQL) return;
    g_hDatabase.Query(SQL_Default_Callback, "SET NAMES 'utf8'", 01);
    g_hDatabase.Query(SQL_Default_Callback, "SET CHARSET 'utf8'", 02);
    g_hDatabase.SetCharset("utf8");

    OnMapStart();
}

//Default callback
public void SQL_Default_Callback(Database hDatabase, DBResultSet hResult, const char[] szError, any QueryID)
{
    if (hResult == null || szError[0]) LogError("SQL_Default_Callback #%i: %s", QueryID, szError);
    return;
}

//Del suspect
void DelSuspect(char[] szAuth)
{
    char szQuery[256];
    FormatEx(SZF(szQuery), "DELETE FROM `suspects_list` WHERE `steam_id` = '%s';", szAuth);
    g_hDatabase.Query(SQL_Default_Callback, szQuery, 1);

    FormatEx(SZF(szQuery), "#%s", szAuth);
    int iDelVip = FindTarget(0, szQuery, true);
    if(iDelVip != -1)
    {
        for(int i = 0; i < 7; ++i) g_sPlayers[iDelVip][i][0] = 0;
    }
}

//Get player data
public void SQL_GetUserData_Callback(Database hDatabase, DBResultSet hResult, const char[] szError, any UserID)
{
    if (hResult == null || szError[0])
    {
        LogError("SQL_GetPlayeData_Callback: %s", szError);
        return;
    }

    if(!UserID) return;
    int iClient = GetClientOfUserId(UserID);
    if(hResult.FetchRow())
        for(int i = 0; i < 7; ++i) hResult.FetchString(i, g_sPlayers[iClient][i], sizeof(g_sPlayers[][]));
    else
        for(int i = 0; i < 7; ++i) g_sPlayers[iClient][i][0] = 0;

    char szDate[32];
    FormatTime(SZF(szDate), "%d.%m.%Y, %H:%M", StringToInt(g_sPlayers[iClient][1]));
    strcopy(g_sPlayers[iClient][1], sizeof(g_sPlayers[][]), szDate);
}

//All suspects
public void SQL_AllSuspect_Callback(Database hDatabase, DBResultSet hResult, const char[] szError, any UserID)
{
    if (hResult == null || szError[0])
    {
        LogError("SQL_AllSuspect_Callback: %s", szError);
        return;
    }

    if(!UserID) return;
    Menu hMenu = new Menu(Handle_AllSuspectMenu);
    hMenu.SetTitle("%t", "mAllSuspect_Title", g_iDelSuspDay);

    int	iClient = GetClientOfUserId(UserID);
    char szArr[5][128],
         szBuffer[512];

    while(hResult.FetchRow())
    {
        hResult.FetchString(0, szArr[0], sizeof(szArr[]));
        hResult.FetchString(2, szArr[1], sizeof(szArr[]));
        hResult.FetchString(3, szArr[2], sizeof(szArr[]));
        hResult.FetchString(4, szArr[3], sizeof(szArr[]));
        hResult.FetchString(6, szArr[4], sizeof(szArr[]));

        FormatEx(SZF(szBuffer), "%s%i%s%s%s%i%s", szArr[0], hResult.FetchInt(1), szArr[1], szArr[2], szArr[3], hResult.FetchInt(5), szArr[4]);
        hMenu.AddItem(szBuffer, szArr[4]);
    }

    if(!hMenu.ItemCount)
    {
        FormatEx(SZF(g_sTranslite), "%T", "NoPlayers", iClient);
        hMenu.AddItem(NULL_STRING, g_sTranslite, ITEMDRAW_DISABLED);
    }

    hMenu.ExitBackButton = true;
    hMenu.ExitButton = true;
    hMenu.Display(iClient, MENU_TIME_FOREVER);
}