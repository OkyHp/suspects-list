//Load file
void LoadSLFile()
{
	g_hReasons.Clear();

	char 	szBuffer[PLATFORM_MAX_PATH],
			szReasons[2][256];
	BuildPath(Path_SM, SZF(szBuffer), "configs/SuspectsList.txt");
	File hFile = OpenFile(szBuffer, "r");
	if(hFile == null) SetFailState("No found file: addons/sourcemod/configs/SuspectsList.txt");
	
	while(!hFile.EndOfFile() && hFile.ReadLine(SZF(szBuffer)))
	{
		TrimString(szBuffer);
		if(!szBuffer[0] || szBuffer[0]=='/') continue;

		ExplodeString(szBuffer, "|", SZF(szReasons), sizeof(szReasons[]));
		for(int i=0; i < 2; ++i) g_hReasons.PushString(szReasons[i]);
	}

	delete hFile;
}

//Get player data
void GetPlayerData(int iClient)
{
    if(!g_hDatabase) return;
    
    char szAuth[32],
         szQuery[256];
    GetClientAuthId(iClient, AuthId_Steam2, SZF(szAuth), true);
    FormatEx(SZF(szQuery), "SELECT * FROM `suspects_list` WHERE `steam_id` = '%s';", szAuth);
    g_hDatabase.Query(SQL_GetUserData_Callback, szQuery, GetClientUserId(iClient));
}

//Get suspect id
int GetSuspect(int iClient)
{
    if(GetEntProp(iClient, Prop_Send, "m_iObserverMode")==6 || !(GetUserFlagBits(iClient) & ADMFLAG_BAN || GetUserFlagBits(iClient) & ADMFLAG_ROOT)) return -1;
    
    int iTarget = GetEntPropEnt(iClient, Prop_Send, "m_hObserverTarget");
    if(iTarget < 65) return iTarget;
    return -1;
}

//Del suspect if ban
void IfBanDelSuspect(int iTarget, char[] SteamID)
{
    if(iTarget == 0 || !g_sPlayers[iTarget][0][0]) return;
    char szQuery[256];
    FormatEx(SZF(szQuery), "DELETE FROM `suspects_list` WHERE `steam_id` = '%s';", SteamID);
    g_hDatabase.Query(SQL_Default_Callback, szQuery, 4);
}

//any
void OpenMainMenu(int iClient)
{
    MainMenu(iClient);
    CGOPrintToChat(iClient, "%t", "OpenSL_MenuMessage");
}

/*** PTaHa magic ***/
char[] IntToStringInt(int iValue)
{
    char sStringInt[6];
    #emit break
    #emit load.s.pri iValue
    #emit stor.s.pri sStringInt
    
    for(int i = 0; i != 4; i++)
    {
        if(sStringInt[i] == 0)
        {
            sStringInt[4] |= (1<<i);
            sStringInt[i] = 0xFF;
        }
    }
    
    return sStringInt;
}

int StringIntToInt(char[] sValue)
{
    char sStringInt[5];

    #emit break
    #emit addr.alt sStringInt
    #emit load.s.pri sValue
    #emit movs 0x5
    
    for(int i = 0; i != 4; i++)
    {
        if(sStringInt[4] & (1<<i))
        {
            sStringInt[i] = 0;
        }
    }
    
    #emit break
    #emit load.s.pri sStringInt
    #emit stack 0x8
    #emit retn  

    return 0;
}

//Del 4 byte symbol
void GetFixNamePlayer(int iClient, char[] sName, int iLen)
{
    GetClientName(iClient, sName, iLen);
    for(int i = 0, len = strlen(sName), CharBytes; i < len;)
    {
        if((CharBytes = GetCharBytes(sName[i])) == 4)
        {
            len -= 4;
            for(int u = i; u <= len; u++) sName[u] = sName[u+4];
        }
        else i += CharBytes;
    }
    if(strlen(sName) < 3) strcopy(sName, iLen, "unknown");
}

//Autizm
bool DelAccess(int iClient, const char[] szAuth)
{
    int iFlag = GetUserFlagBits(iClient);
    if(iFlag & ADMFLAG_ROOT) return true;

    if(g_sAdminFlag[0] == '+')
    {
        char szAuthAdmin[32];
        GetClientAuthId(iClient, AuthId_Steam2, SZF(szAuthAdmin), true);
        if(!strcmp(szAuth, szAuthAdmin)) return true;
    }
    else if(g_sAdminFlag[0] != '-' && (iFlag & ReadFlagString(g_sAdminFlag))) return true;

    return false;
}

bool IsSringNumeric(const char[] szBuffer)
{
    int i = 0;
    while (szBuffer[i])
    {
        if(!IsCharNumeric(szBuffer[i])) return false;
        ++i;
    }
    return true;
}

void ResetCR_Buffer()
{
    g_iCR_UID = 0;
    for (int i = 0; i < 3; ++i) g_sCustomReason[i][0] = 0;
}