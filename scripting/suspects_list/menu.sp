/*** Menu ***/

//Main menu
void MainMenu(int iClient) 
{ 
    char szName[64];

    Menu hMenu = new Menu(Handle_MainMenu);
    hMenu.SetTitle("%t", "mMain_Title");

    SetGlobalTransTarget(iClient);
    FormatEx(SZF(g_sTranslite), "%t", "mMain_AllSuspectItem");
    hMenu.AddItem(NULL_STRING, g_sTranslite);

    for(int i = 1; i <= MaxClients; ++i)
    {
        if(!IsClientInGame(i) || IsFakeClient(i)) continue;

        int iFlag = GetUserFlagBits(i);
        if(iFlag & ADMFLAG_ROOT || iFlag & ADMFLAG_BAN) continue;
            
        FormatEx(SZF(szName), "%N", i);
        hMenu.AddItem(IntToStringInt(GetClientUserId(i)), szName);
    }

    if(hMenu.ItemCount == 1)
    {
        FormatEx(SZF(g_sTranslite), "%t", "NoPlayers");
        hMenu.AddItem(NULL_STRING, g_sTranslite, ITEMDRAW_DISABLED);
    }

    hMenu.ExitButton = true;
    hMenu.Display(iClient, MENU_TIME_FOREVER);
}

//Calback main menu
public int Handle_MainMenu(Menu hMenu, MenuAction action, int iClient, int iItem)
{
    switch(action)
    {
        case MenuAction_End: delete hMenu;
        case MenuAction_Select:
        {
            if(iItem == 0)
            {
                g_hDatabase.Query(SQL_AllSuspect_Callback, "SELECT * FROM `suspects_list` ORDER BY `add_time` DESC;", GetClientUserId(iClient));
            }
            else
            {
                char szUserID[32];
                GetMenuItem(hMenu, iItem, SZF(szUserID));
                SubMenu(iClient, GetClientOfUserId(StringIntToInt(szUserID)));
            }
        }
    }
}

//Submenu menu
void SubMenu(int iClient, int iSuspClient)
{ 
    if(!iSuspClient)
    {
        CGOPrintToChat(iClient, "%t", "PlayerOut");
        return;
    }

    Menu hMenu = new Menu(Handle_SubMenu);

    if(g_sPlayers[iSuspClient][0][0]) FormatEx(SZF(g_sTranslite), "%t", "mSub_TitleInfo", iSuspClient, g_sPlayers[iSuspClient][2], g_sPlayers[iSuspClient][3], g_sPlayers[iSuspClient][1]);
    else g_sTranslite[0] = 0;

    hMenu.SetTitle("%t", "mSub_Title", g_sTranslite);

    SetGlobalTransTarget(iClient);

    FormatEx(SZF(g_sTranslite), "%t", "AddSuspectItem");
    hMenu.AddItem(IntToStringInt(GetClientUserId(iSuspClient)), g_sTranslite, !g_sPlayers[iSuspClient][0][0] ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED );

    FormatEx(SZF(g_sTranslite), "%t", "DeleteSuspectItem");
    hMenu.AddItem(IntToStringInt(GetClientUserId(iSuspClient)), g_sTranslite, (g_sPlayers[iSuspClient][0][0] && DelAccess(iClient, g_sPlayers[iSuspClient][4])) ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED );

    hMenu.ExitBackButton = true;
    hMenu.ExitButton = true;
    hMenu.Display(iClient, MENU_TIME_FOREVER);
}

//Calback submenu menu
public int Handle_SubMenu(Menu hMenu, MenuAction action, int iClient, int iItem)
{
    switch(action)
    {
        case MenuAction_End: delete hMenu;
        case MenuAction_Cancel: if(iItem == MenuCancel_ExitBack) MainMenu(iClient);
        case MenuAction_Select:
        {
            char szUserID[8];
            GetMenuItem(hMenu, iItem, SZF(szUserID));

            int iSuspClient = GetClientOfUserId(StringIntToInt(szUserID));
            if(iSuspClient)
            {
                switch(iItem)
                {
                    case 0: ReasonMenu(iClient, szUserID);
                    case 1: 
                    {
                        char szAuth[32];

                        GetClientAuthId(iSuspClient, AuthId_Steam2, SZF(szAuth), true);
                        DelSuspect(szAuth);

                        CGOPrintToChat(iClient, "%t", "DeleteSuspect_1", iSuspClient);
                    }
                }
            }
            else CGOPrintToChat(iClient, "%t", "PlayerOut");
        }
    }
}

//Reason menu
void ReasonMenu(int iClient, char[] szUserID) 
{ 
    Menu hMenu = new Menu(Handle_ReasonMenu);
    hMenu.SetTitle("%t", "mReason_Title");
    
    int aCount = (g_hReasons.Length);
    char szReason[256],
         szTime[12],
         szBuffer[512];

    FormatEx(SZF(szReason), "%T", "CustomReason", iClient);
    hMenu.AddItem(szUserID, szReason);

    for (int i = 0; i < aCount; i++)
    {
        g_hReasons.GetString(i, SZF(szReason));
        g_hReasons.GetString(++i, SZF(szTime));
        FormatEx(SZF(szBuffer), "%s%s%s", szUserID, szTime, szReason);
        hMenu.AddItem(szBuffer, szReason);
    }

    hMenu.ExitBackButton = true;
    hMenu.ExitButton = true;
    hMenu.Display(iClient, MENU_TIME_FOREVER);
}

//Calback submenu menu
public int Handle_ReasonMenu(Menu hMenu, MenuAction action, int iClient, int iItem)
{
    switch(action)
    {
        case MenuAction_End: delete hMenu;
        case MenuAction_Cancel: if(iItem == MenuCancel_ExitBack) MainMenu(iClient);
        case MenuAction_Select:
        {
            switch(iItem)
            {
                case 0: 
                {
                    if(g_sCustomReason[0][0])
                    {
                        CGOPrintToChat(iClient, "%t", "CR_BufferInUse");
                        return 0;
                    }

                    g_iCR_UID = GetClientUserId(iClient);

                    char szBuffer[128];
                    GetMenuItem(hMenu, iItem, SZF(szBuffer));
                    strcopy(g_sCustomReason[0], sizeof(g_sCustomReason[]), szBuffer);

                    CGOPrintToChat(iClient, "%t", "CR_WriteInChatReason");
                }
                default:
                {
                    char szBuffer[512],
                         szArray[3][PLATFORM_MAX_PATH];
                    GetMenuItem(hMenu, iItem, SZF(szBuffer));
                    // int p1 = FindCharInString(szBuffer, '');
                    // szBuffer[p1] = 0; ++p1;
                    // int p2 = FindCharInString(szBuffer[p1], '')+p1;
                    // szBuffer[p2] = 0; ++p2;
                    ExplodeString(szBuffer, "", SZF(szArray), sizeof(szArray[]));

                    AddSuspectQuery(iClient, szArray);
                }
            }
        }
    }
    return 0;
}

//Callback all suspect
public int Handle_AllSuspectMenu(Menu hMenu, MenuAction action, int iClient, int iItem)
{
    switch(action)
    {
        case MenuAction_End: delete hMenu;
        case MenuAction_Cancel: if(iItem == MenuCancel_ExitBack) MainMenu(iClient);
        case MenuAction_Select:
        {
            char szBuffer[512],
                 szArr[7][128],
                 szAuthAdmin[32],
                 szDate[32];
            GetMenuItem(hMenu, iItem, SZF(szBuffer));
            ExplodeString(szBuffer, "", SZF(szArr), sizeof(szArr[]));
            GetClientAuthId(iClient, AuthId_Steam2, SZF(szAuthAdmin), true);
            FormatTime(SZF(szDate), "%d.%m.%Y, %H:%M", StringToInt(szArr[1]));

            Menu hMenu2 = new Menu(Handle_AllSuspectMenu_Sub);
            hMenu2.SetTitle("%t", "mInfo_Title", szArr[6], szArr[2], szArr[3], szDate);
            
            FormatEx(SZF(g_sTranslite), "%T", "DeleteSuspectItem", iClient);
            hMenu2.AddItem(szArr[0], g_sTranslite, DelAccess(iClient, szArr[4]) ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED );

            hMenu2.ExitBackButton = true;
            hMenu2.ExitButton = true;
            hMenu2.Display(iClient, MENU_TIME_FOREVER);
        }
    }
}

//Callback all sub suspect menu
public int Handle_AllSuspectMenu_Sub(Menu hMenu, MenuAction action, int iClient, int iItem)
{
    switch(action)
    {
        case MenuAction_End: delete hMenu;
        case MenuAction_Cancel: if(iItem == MenuCancel_ExitBack) MainMenu(iClient);
        case MenuAction_Select:
        {
            char szAuth[32];
            GetMenuItem(hMenu, iItem, SZF(szAuth));
            DelSuspect(szAuth);

            CGOPrintToChat(iClient, "%t", "DeleteSuspect_2", szAuth);
        }
    }
}