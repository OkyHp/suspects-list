#include <sourcemod>
#include <csgo_colors>
#undef REQUIRE_PLUGIN
#include <adminmenu>
#include <materialadmin>
#pragma semicolon 1
#pragma newdecls required

#define SZF(%0) %0, sizeof(%0)

int 		g_iDelSuspDay,
			g_iCR_UID;
bool		g_bMySQL;
char		g_sPlayers[MAXPLAYERS+1][7][PLATFORM_MAX_PATH],
			g_sTranslite[PLATFORM_MAX_PATH],
			g_sCustomReason[3][PLATFORM_MAX_PATH],
			g_sAdminFlag[12];
Database	g_hDatabase;
ArrayList	g_hReasons;
TopMenu		g_hAdminMenu = null;

#include "suspects_list/others.sp"
#include "suspects_list/database.sp"
#include "suspects_list/menu.sp"

public Plugin myinfo =
{
	name        = 	"Suspects List",
	author      = 	"OkyHp",
	version     = 	"1.1.0",
	url         = 	"https://dev-source.ru/, https://hlmod.ru/"
};

public void OnPluginStart()
{
	if(GetEngineVersion() != Engine_CSGO) SetFailState("This plugin works only on CS:GO");

	ConVar CVAR;
	(CVAR = CreateConVar("sl_del_time_susp", "7", "Через сколько дней удалить подозреваемого. 0 - Выключить", _, true, 0.0, true, 90.0)).AddChangeHook(ChangeCvar_DelTime);
	g_iDelSuspDay = CVAR.IntValue;
	(CVAR = CreateConVar("sl_del_access_flag", "d", "Флаг доступа для удаления подозреваемых. \n'+' - Удалять подозреваемых может только админ кторый его добавил. \n'-' - Выключить возможность удалять всем. (У рута иммунитет для всех вариантов)")).AddChangeHook(ChangeCvar_AdminFlag);
	CVAR.GetString(SZF(g_sAdminFlag));
	AutoExecConfig(true, "SuspectsList");

	LoadTranslations("SuspectsList.phrases");

	RegAdminCmd("sm_sl", SuspectsList_Callback, ADMFLAG_BAN);
	RegAdminCmd("sm_sl_ban", SpBan_Callback, ADMFLAG_BAN);
	RegAdminCmd("sm_sl_reload", Reload_Callback, ADMFLAG_ROOT);

	g_hReasons = new ArrayList(ByteCountToCells(256));

	LoadSLFile();
	DatabesConnect();

	if(LibraryExists("adminmenu"))
    {
        TopMenu hTopMenu;
        if((hTopMenu = GetAdminTopMenu()) != null) OnAdminMenuReady(hTopMenu);
    }
}

public void ChangeCvar_DelTime(ConVar convar, const char[] oldValue, const char[] newValue)
{
	g_iDelSuspDay = convar.IntValue;
}

public void ChangeCvar_AdminFlag(ConVar convar, const char[] oldValue, const char[] newValue)
{
	convar.GetString(SZF(g_sAdminFlag));
}

public void OnMapStart()
{
	if(!g_hDatabase) return;
	
	char szQuery[256];
	FormatEx(SZF(szQuery), "DELETE FROM `suspects_list` WHERE `add_time` < %i;", (GetTime()-(g_iDelSuspDay * 24 * 60 * 60)) );
	g_hDatabase.Query(SQL_Default_Callback, szQuery, 3);
}

/*** Admin-menu ***/
public void OnLibraryRemoved(const char[] szName)
{
    if(StrEqual(szName, "adminmenu")) g_hAdminMenu = null;
}

public void OnAdminMenuReady(Handle aTopMenu)
{
	TopMenu hTopMenu = TopMenu.FromHandle(aTopMenu);
	if(hTopMenu == g_hAdminMenu) return;
	g_hAdminMenu = hTopMenu;

	TopMenuObject hCategory = g_hAdminMenu.FindCategory("PlayerCommands");
	if(hCategory != INVALID_TOPMENUOBJECT) g_hAdminMenu.AddItem("suspects_list_item", Handler_MenuSuspectList, hCategory, "suspects_list", ADMFLAG_BAN);
}

public void Handler_MenuSuspectList(TopMenu hMenu, TopMenuAction action, TopMenuObject object_id, int iClient, char[] sBuffer, int maxlength)
{
	switch (action)
    {
    	case TopMenuAction_DisplayOption: FormatEx(sBuffer, maxlength, "%T", "SuspectsList", iClient);
    	case TopMenuAction_SelectOption: OpenMainMenu(iClient);
    }
}

/*** Actions ***/
//Reload conf & db connect
public Action Reload_Callback(int iClient, int iArgs) 
{ 
	LoadSLFile();
	if(g_hDatabase) delete g_hDatabase;
	DatabesConnect();

	for(int i = 1; i <= MaxClients; ++i) if(IsClientInGame(i) && !IsFakeClient(i)) GetPlayerData(i);

	CGOPrintToChat(iClient, "%t", "ConfReloadMessage");
	return Plugin_Handled;
}

//Main menu
public Action SuspectsList_Callback(int iClient, int iArgs) 
{ 
	OpenMainMenu(iClient);
	return Plugin_Handled;
}

//Ban player
public Action SpBan_Callback(int iClient, int iArgs) 
{
	int iTarget = GetSuspect(iClient);
	if(iTarget != -1 && g_sPlayers[iTarget][0][0])
	{
		char szAuth[32];
		GetClientAuthId(iTarget, AuthId_Steam2, SZF(szAuth), true);
		IfBanDelSuspect(iTarget, szAuth);

		if(GetFeatureStatus(FeatureType_Native, "MABanPlayer") == FeatureStatus_Available)
			MABanPlayer(iClient, iTarget, MA_BAN_STEAM, StringToInt(g_sPlayers[iTarget][5]), g_sPlayers[iTarget][2]);
		else
			ServerCommand("sm_ban #%i %s \"%s\"", GetClientUserId(iTarget), g_sPlayers[iTarget][5], g_sPlayers[iTarget][2]);
	}
	return Plugin_Handled;
}

//Player connect
public void OnClientPutInServer(int iClient)
{
	GetPlayerData(iClient);
}

//Observation event
public Action OnPlayerRunCmd(int iClient, int& buttons, int& impulse, float vel[3], float angles[3], int& weapon, int& subtype, int& cmdnum, int& tickcount, int& seed, int mouse[2])
{
	int iTarget = GetSuspect(iClient);
	if(iTarget != -1 && g_sPlayers[iTarget][0][0]) CGOPrintCenterText(iClient, "%t", "HudMessage", g_sPlayers[iTarget][2], g_sPlayers[iTarget][1], g_sPlayers[iTarget][3]);
}

//MA
public void MAOnClientBanned(int iClient, int iTarget, char[] sIp, char[] sSteamID, char[] sName, int iTime, char[] sReason)
{
	IfBanDelSuspect(iTarget, sSteamID);
}

//Action client say
public Action OnClientSayCommand(int iClient, const char[] command, const char[] sArgs)
{
    if(!iClient) return Plugin_Continue;
    int iCR_Client = GetClientOfUserId(g_iCR_UID);
    if(!iCR_Client || iCR_Client != iClient) return Plugin_Continue;

    if(!strcmp(sArgs, "N") || !strcmp(sArgs, "n"))
    {
        CGOPrintToChat(iClient, "%t", "CR_Back");
        ResetCR_Buffer();
        return Plugin_Handled;
    }

    if(!g_sCustomReason[2][0])
    {
        strcopy(g_sCustomReason[2], sizeof(g_sCustomReason[]), sArgs);
        CGOPrintToChat(iClient, "%t", "CR_PrintReason", sArgs);
        CGOPrintToChat(iClient, "%t", "CR_WriteInChatTime");
    }
    else
    {
        if(!IsSringNumeric(sArgs))
        {
            CGOPrintToChat(iClient, "%t", "IsNotNumeric");
            return Plugin_Handled;
        }
        strcopy(g_sCustomReason[1], sizeof(g_sCustomReason[]), sArgs);
        CGOPrintToChat(iClient, "%t", "CR_PrintTime", sArgs);

        AddSuspectQuery(iClient, g_sCustomReason);
        ResetCR_Buffer();
    }
    return Plugin_Handled;
}

void AddSuspectQuery(int iClient, char[][] szBuffer)
{
    char szQuery[512],
         szAuth[32],
         szAuthSuspect[32],
         szName[MAX_NAME_LENGTH*2+1],
         szSuspName[MAX_NAME_LENGTH*2+1],
         szReason[PLATFORM_MAX_PATH*2+1];

    int iSuspClient = GetClientOfUserId(StringIntToInt(szBuffer[0]));
    if(!iSuspClient)
    {
        CGOPrintToChat(iClient, "%t", "PlayerOut");
        return;
    }

    GetClientAuthId(iSuspClient, AuthId_Steam2, SZF(szAuthSuspect), true);

    GetFixNamePlayer(iClient, szQuery, MAX_NAME_LENGTH);
    g_hDatabase.Escape(szQuery, SZF(szName)); 
    GetFixNamePlayer(iSuspClient, szQuery, MAX_NAME_LENGTH);
    g_hDatabase.Escape(szQuery, SZF(szSuspName));
    g_hDatabase.Escape(szBuffer[2], SZF(szReason));


    GetClientAuthId(iClient, AuthId_Steam2, SZF(szAuth), true);

    Format(SZF(szQuery), "INSERT INTO `suspects_list` (`steam_id`, `add_time`, `reason`, `add_adm_name`, `add_adm_steam_id`, `ban_time`, `susp_name`) VALUES ('%s', %i, '%s', '%s', '%s', '%s', '%s');", szAuthSuspect, GetTime(), szReason, szName, szAuth, szBuffer[1], szSuspName);
    g_hDatabase.Query(SQL_Default_Callback, szQuery, 2);

    FormatTime(SZF(szQuery), "%d.%m.%Y, %H:%M", GetTime());
    strcopy(g_sPlayers[iSuspClient][0], sizeof(g_sPlayers[][]), szAuthSuspect);
    strcopy(g_sPlayers[iSuspClient][1], sizeof(g_sPlayers[][]), szQuery);
    strcopy(g_sPlayers[iSuspClient][2], sizeof(g_sPlayers[][]), szReason);
    strcopy(g_sPlayers[iSuspClient][3], sizeof(g_sPlayers[][]), szName);
    strcopy(g_sPlayers[iSuspClient][4], sizeof(g_sPlayers[][]), szAuth);
    strcopy(g_sPlayers[iSuspClient][5], sizeof(g_sPlayers[][]), szBuffer[1]);
    strcopy(g_sPlayers[iSuspClient][6], sizeof(g_sPlayers[][]), szSuspName);

    CGOPrintToChat(iClient, "%t", "AddSuspect", iSuspClient);
}